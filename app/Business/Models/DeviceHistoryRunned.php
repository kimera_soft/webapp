<?php
namespace App\Business\Models;

use Illuminate\Database\Eloquent\Model;

class DeviceHistoryRunned extends Model
{
     protected $table = "device_history_runned";


	  /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['device_id'];


     /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;
}

