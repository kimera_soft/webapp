<?php
namespace App\Business\Models;

use Illuminate\Database\Eloquent\Model;

class Plantation extends Model
{

	protected $table = "plantations";

	  /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'min_temperature', 'max_temperature', 'min_moisture', 'max_moisture', 'user_id'];


     /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;
}
