<?php
namespace App\Business\Models;

use Illuminate\Database\Eloquent\Model;

class DeviceHistoryData extends Model
{

	protected $table = "device_history_data";


	  /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['temperature', 'moisture', 'device_id'];


     /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;
}
