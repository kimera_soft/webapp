<?php
namespace App\Business\Models;

use Illuminate\Database\Eloquent\Model;

class Temperature extends Model
{

	protected $table = "temperatures";


	  /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['valor'];


     /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;
}
