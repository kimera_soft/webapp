<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Response;
use App\Http\Controllers\Controller;
use App\Business\Models\Plantation;

class PlantationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('plantation.index');
    }

    public function list_devices()
    {
    	$plantations = Plantation::all();

    	return response()->json($plantations);
    }

    public function show($id)
    {
    	$plantation = Plantation::where('id', $id)->first();
    	return view('plantation.show', ['plantation' => $plantation]);
    }
}
