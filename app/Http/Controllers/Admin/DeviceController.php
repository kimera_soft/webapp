<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Response;
use App\Business\Models\Device;

class DeviceController extends Controller
{
    public function index()
    {
    	return view('device.index');
    }

    public function list_devices()
    {
    	$devices = Device::all();

    	return response()->json($devices);
    }

    public function show($id)
    {
    	$device = Device::where('id', $id)->first();
    	return view('device.show', ['device' => $device]);
    }

    public function list_by_plantation($plantation_id)
    {
    	$devices = Device::where('plantation_id', $plantation_id)->get();
    	return response()->json($devices);	
    }
}
