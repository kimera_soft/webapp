<?php

namespace App\Http\Controllers\Apis;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Business\Models\DeviceHistoryRunned;
use DB;
class DeviceHistoryRunnedController extends Controller
{
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = ['device_id' => $request->input('device_id')];
        DeviceHistoryRunned::create($input);
        return response()->json(["runned" => "true"]);
    }


    public function list_monthly_runned($id)
    {
        $year = date('Y');
        $data = DB::select(DB::raw("select count(*) as cantidad from device_history_runned where device_id = $id and YEAR(created_at) = $year group by MONTH(created_at)"));
        $arrayData = [];
        foreach ($data as $value) {
            $arrayData[] = intval(($value->cantidad));
        }

        return response()->json($arrayData);
    }

    public function list_monthly_runned_by_plantation($plantation_id)
    {
        $year = date('Y');
        $data = DB::select(DB::raw("select count(*) as cantidad from device_history_runned runned join devices device on device.id = runned.device_id join plantations plantation on plantation.id = device.plantation_id where plantation.id = $plantation_id and YEAR(runned.created_at) = $year group by MONTH(runned.created_at);"));
        $arrayData = [];
        foreach ($data as $value) {
            $arrayData[] = intval(($value->cantidad));
        }

        return response()->json($arrayData);
    }
// select count(*) from device_history_runned runned join devices device on device.id = runned.device_id join plantations plantation on plantation.id = device.plantation_id where plantation.id = 1 and YEAR(runned.created_at) = 2016 group by MONTH(runned.created_at);

    public function list_device_runned($id)
    {
        $data = DeviceHistoryRunned::where('device_id', $id)->get();

        return response()->json($data);
    }
}
