<?php

namespace App\Http\Controllers\Apis;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests;
use App\Business\Models\DeviceHistoryData;

class DeviceHistoryDataController extends Controller
{
    
    public function index()
    {
        return "hola";
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $input = [
                'device_id' => $request->input('device_id'),
                'moisture' => floatval($request->input('moisture')), 
                'temperature' => floatval($request->input('temperature'))];

        $deviceHistoryData = DeviceHistoryData::create($input);
        return response()->json(DeviceHistoryData::all());
    }

    public function list_history_data($id)
    {
        $deviceHistoryDataList = DeviceHistoryData::where('device_id', $id)->orderBy('created_at', 'desc')->get();
        return response()->json($deviceHistoryDataList);
    }
}
