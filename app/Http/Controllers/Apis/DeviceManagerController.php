<?php

namespace App\Http\Controllers\Apis;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Business\Models\DeviceHistoryData;
use App\Business\Models\Device;
use App\Business\Models\Plantation;
use App\Business\Models\Temperature;
class DeviceManagerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function has_to_run($id)
    {
        $device = Device::where('id', $id)->first();
        $plantation = Plantation::where('id', $device->plantation_id)->first();
        $lastTenUpdates = DeviceHistoryData::where('device_id',  $id)->orderBy('created_at', 'desc')->take(10)->get();
        
        $temperatureSum = 0;
        $moistureSum = 0;

        foreach ($lastTenUpdates as $lastUpdate) {
            $temperatureSum += floatval($lastUpdate->temperature);
            $moistureSum += floatval($lastUpdate->moisture);
        }

        $temperatureAverage = $temperatureSum / 10;
        $moistureAverage = $moistureSum / 10;

        if($temperatureAverage > $plantation->max_temperature || $moistureAverage > $plantation->max_moisture){
            return response()->json(['hasToRun' => true]);
        }
        else{
            return response()->json(['hasToRun' => false]);
        }
    }

    public function get_device($id)
    {
        $device = Device::where('id', $id)->get();

        return response()->json($device);
    }


    public function temperature()
    {
        $temperature = Temperature::orderBy('created_at', 'desc')->get()->first();
        
        $value = 0;
        if($temperature != null){
            $value = $temperature->valor;
        } 
        
        if ($value == null) {
            $value = 2;
        }

        return response()->json(["temperature" => $value]);
    }

    public function change_temperature($id)
    {   
         $input = ['valor' => $id];

        Temperature::create($input);
        return response()->json(['success' => true]);
    }
}