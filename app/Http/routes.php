<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    //

    Route::get('/home/welcome', 'Home\HomeController@welcome');
    Route::get('/home/dashboard', 'Home\HomeController@login' );
    
    //Devices
    Route::get('devices', 'Admin\DeviceController@index');
	Route::get('devices/list', 'Admin\DeviceController@list_devices');
	Route::get('devices/{id}', 'Admin\DeviceController@show');
	Route::get('devices/list_by_plantation/{plantation_id}', 'Admin\DeviceController@list_by_plantation');

	Route::get('plantations', 'Admin\PlantationController@index');
	Route::get('plantations/list', 'Admin\PlantationController@list_devices');
	Route::get('plantations/{id}', 'Admin\PlantationController@show');

    Route::resource('DeviceHistoryData', 'Apis\DeviceHistoryDataController');
    Route::get('DeviceHistoryData/list_history_data/{id}', 'Apis\DeviceHistoryDataController@list_history_data');
    Route::get('DeviceHistoryData/list_monthly_runned_by_plantation/{plantation_id}', 'Apis\DeviceHistoryDataController@list_monthly_runned_by_plantation');

    //Device History Runned
    Route::resource('DeviceHistoryRunned', 'Apis\DeviceHistoryRunnedController');
    Route::get('DeviceHistoryRunned/list_monthly_runned/{id}', 'Apis\DeviceHistoryRunnedController@list_monthly_runned');
    Route::get('DeviceHistoryRunned/list_device_runned/{id}', 'Apis\DeviceHistoryRunnedController@list_device_runned');
	Route::get('DeviceHistoryRunned/by_plantation/{plantation_id}', 'Apis\DeviceHistoryRunnedController@list_monthly_runned_by_plantation');

    Route::get('DeviceManager/has_to_run/{id}', 'Apis\DeviceManagerController@has_to_run');
    Route::get('DeviceManager/temperature', 'Apis\DeviceManagerController@temperature');
    Route::get('DeviceManager/change_temperature/{id}', 'Apis\DeviceManagerController@change_temperature');
    //Route::resource('DeviceManager', 'Apis\DeviceManagerController');
});
