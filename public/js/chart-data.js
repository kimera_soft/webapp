var randomScalingFactor = function(){ return Math.round(Math.random()*1000)};
		
	var lineChartData = {
			labels : ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
			datasets : [
				{
					label: "My First dataset",
					fillColor : "rgba(220,220,220,0.2)",
					strokeColor : "rgba(220,220,220,1)",
					pointColor : "rgba(220,220,220,1)",
					pointStrokeColor : "#fff",
					pointHighlightFill : "#fff",
					pointHighlightStroke : "rgba(220,220,220,1)"
				},
			]

		}

	var lineChartData2 = {
			labels : ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
			datasets : [
				{
					label: "My First dataset",
					fillColor : "rgba(220,220,220,0.2)",
					strokeColor : "rgba(220,220,220,1)",
					pointColor : "rgba(220,220,220,1)",
					pointStrokeColor : "#fff",
					pointHighlightFill : "#fff",
					pointHighlightStroke : "rgba(220,220,220,1)"
				},
			]

		}	
		
	// var barChartData = {
	// 		labels : ["January","February","March","April","May","June","July"],
	// 		datasets : [
	// 			{
	// 				fillColor : "rgba(220,220,220,0.5)",
	// 				strokeColor : "rgba(220,220,220,0.8)",
	// 				highlightFill: "rgba(220,220,220,0.75)",
	// 				highlightStroke: "rgba(220,220,220,1)",
	// 				data : [randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor()]
	// 			},
	// 			{
	// 				fillColor : "rgba(48, 164, 255, 0.2)",
	// 				strokeColor : "rgba(48, 164, 255, 0.8)",
	// 				highlightFill : "rgba(48, 164, 255, 0.75)",
	// 				highlightStroke : "rgba(48, 164, 255, 1)",
	// 				data : [randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor()]
	// 			}
	// 		]
	
	// 	}

	// var pieData = [
	// 			{
	// 				value: 300,
	// 				color:"#30a5ff",
	// 				highlight: "#62b9fb",
	// 				label: "Blue"
	// 			},
	// 			{
	// 				value: 50,
	// 				color: "#ffb53e",
	// 				highlight: "#fac878",
	// 				label: "Orange"
	// 			},
	// 			{
	// 				value: 100,
	// 				color: "#1ebfae",
	// 				highlight: "#3cdfce",
	// 				label: "Teal"
	// 			},
	// 			{
	// 				value: 120,
	// 				color: "#f9243f",
	// 				highlight: "#f6495f",
	// 				label: "Red"
	// 			}

	// 		];
			
	// var doughnutData = [
	// 				{
	// 					value: 300,
	// 					color:"#30a5ff",
	// 					highlight: "#62b9fb",
	// 					label: "Blue"
	// 				},
	// 				{
	// 					value: 50,
	// 					color: "#ffb53e",
	// 					highlight: "#fac878",
	// 					label: "Orange"
	// 				},
	// 				{
	// 					value: 100,
	// 					color: "#1ebfae",
	// 					highlight: "#3cdfce",
	// 					label: "Teal"
	// 				},
	// 				{
	// 					value: 120,
	// 					color: "#f9243f",
	// 					highlight: "#f6495f",
	// 					label: "Red"
	// 				}
	
	// 			];

window.onload = function(){
	var line_chart = document.getElementById("line-chart");
	if (line_chart) {
		var chart1 = line_chart.getContext("2d");
	
		$.get('http://192.168.43.17:8001/DeviceHistoryRunned/list_monthly_runned/' + device_id, function(response){
			console.log(lineChartData);
			lineChartData.datasets[0].data = response;
			window.myLine = new Chart(chart1).Line(lineChartData, {
			responsive: true
			});
		});	
	}
	
	var line_chart_plantation = document.getElementById("line-chart_plantation");
	console.log(line_chart_plantation);
	if (line_chart_plantation) {
		var chart2 = line_chart_plantation.getContext("2d");
	
		$.get('http://192.168.43.17:8001/DeviceHistoryRunned/by_plantation/' + plantation_id, function(response){
			console.log(lineChartData2);
			lineChartData2.datasets[0].data = response;
			window.myLine = new Chart(chart2).Line(lineChartData2, {
			responsive: true
			});
		});	
	}
	
	//ine-chart_plantation
	
	// var chart2 = document.getElementById("bar-chart").getContext("2d");
	// window.myBar = new Chart(chart2).Bar(barChartData, {
	// 	responsive : true
	// });
	// var chart3 = document.getElementById("doughnut-chart").getContext("2d");
	// window.myDoughnut = new Chart(chart3).Doughnut(doughnutData, {responsive : true
	// });
	// var chart4 = document.getElementById("pie-chart").getContext("2d");
	// window.myPie = new Chart(chart4).Pie(pieData, {responsive : true
	// });
	
};