@extends('shared.master')

@section('title', 'Dashboard')

@section('content')
   <div class="row">
            <div class="col-lg-12">
                <h2>Eventos</h2>
            </div>
            
            <div class="col-md-12">
                <div class="panel panel-blue">
                    <div class="panel-heading dark-overlay">Hoy todo estuvo bien</div>
                    <div class="panel-body">
                        <p>No hubo ninguna eventualidad</p>
                    </div>
                </div>
            </div><!--/.col-->
            
            <div class="col-md-12">
                <div class="panel panel-teal">
                    <div class="panel-heading dark-overlay">Riego Ejectuado</div>
                    <div class="panel-body">
                        <p>El Riego fue satisfactorio</p>
                    </div>
                </div>
            </div><!--/.col-->
            
            
            <div class="col-md-12">
                <div class="panel panel-teal">
                    <div class="panel-heading dark-overlay">Riego Ejectuado</div>
                    <div class="panel-body">
                        <p>El Riego fue satisfactorio</p>
                    </div>
                </div>
            </div><!--/.col-->
            
            <div class="col-md-12">
                <div class="panel panel-red">
                    <div class="panel-heading dark-overlay">El proceso ejecutado, pero detecto de anomalia</div>
                    <div class="panel-body">
                        <p>En el dia de hoy se presentaron liveles fuera de lo normal de calentamiento y resequedad.
                        Se debe estar bien pendiente a la plantacion</p>
                    </div>
                </div>
            </div><!--/.col-->
        </div><!--/.row-->  
@endsection