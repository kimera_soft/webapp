@extends('shared.master')

@section('title', 'Detalle del Dispositivo: '.$device->name)

@section('content')
	<script>
		var device_id = {{ $device->id }};
	</script>
	<script src="{{ URL::asset('js/jquery-1.11.1.min.js') }}"></script>
	<script src="{{ URL::asset('js/bootstrap-table.js') }}"></script>
	<script src="{{ URL::asset('js/chart-data.js') }}"></script>
	<script src="{{ URL::asset('js/easypiechart-data.js') }}"></script>
	<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-heading"></div>
					<div class="panel-body">
						<div class="canvas-wrapper">
							<canvas class="main-chart" id="line-chart" height="200" width="600"></canvas>
						</div>
					</div>
				</div>
			</div>
		</div><!--/.row-->

	<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-heading">Periodos de Regado</div>
					<div class="panel-body">
						<table data-toggle="table" data-url="/DeviceHistoryRunned/list_device_runned/{{ $device->id }}"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc">
						    <thead>
						    <tr>
						        <th data-field="created_at" data-sortable="true">Fecha / Hora</th>
						    </tr>
						    </thead>
						</table>
					</div>
				</div>
			</div>
		</div><!--/.row-->	

		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-heading">Periodos de Regado</div>
					<div class="panel-body">
						<table data-toggle="table" data-url="/DeviceHistoryData/list_history_data/{{ $device->id }}"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc">
						    <thead>
						    <tr>
						        <th data-field="temperature" data-sortable="true">Temperature</th>
						        <th data-field="moisture" data-sortable="true">Humedad</th>
						        <th data-field="created_at" data-sortable="true">Fecha / Hora</th>
						    </tr>
						    </thead>
						</table>
					</div>
				</div>
			</div>
		</div><!--/.row-->	
	
@endsection