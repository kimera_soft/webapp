@extends('shared.master')

@section('title', 'Plantaciones')

@section('content')
	<script src="js/jquery-1.11.1.min.js"></script>
	<script src="js/bootstrap-table.js"></script>
	<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-heading">Grupos de Plantaciones</div>
					<div class="panel-body">
						<table data-toggle="table" data-url="plantations/list"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc">
						    <thead>
						    <tr>
						        <th data-field="name" data-sortable="true" >Nombre</th>
						        <th data-field="min_temperature" data-sortable="true">Temperatura Minima</th>
						        <th data-field="max_temperature"  data-sortable="true">Temperatura Maxima</th>
						        <th data-field="min_moisture" data-sortable="true">Humedad Minima</th>
						        <th data-field="max_moisture"  data-sortable="true">Humedad Maxima</th>
						        <th data-field="created_at" data-sortable="true">Creado</th>
						    </tr>
						    </thead>
						</table>
					</div>
				</div>
			</div>
		</div><!--/.row-->	
	
@endsection