@extends('shared.master')

@section('title', 'Detalle de la plantacion: '.$plantation->name)

@section('content')
	<script>
		var plantation_id = {{ $plantation->id }};
	</script>
	<script src="{{ URL::asset('js/jquery-1.11.1.min.js') }}"></script>
	<script src="{{ URL::asset('js/bootstrap-table.js') }}"></script>
	<script src="{{ URL::asset('js/chart-data.js') }}"></script>
	
	<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-heading"></div>
					<div class="panel-body">
						<div class="canvas-wrapper">
							<canvas class="main-chart" id="line-chart_plantation" height="200" width="600"></canvas>
						</div>
					</div>
				</div>
			</div>
		</div><!--/.row-->

		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-heading">Dispositivos</div>
					<div class="panel-body">
						<table data-toggle="table" data-url="/devices/list_by_plantation/{{ $plantation->id }}"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc">
						    <thead>
						    <tr>
						        <th data-field="name" data-sortable="true" >Nombre</th>
						        <th data-field="latitude" data-sortable="true">Latitud</th>
						        <th data-field="longitude"  data-sortable="true">Longitud</th>
						        <th data-field="created_at" data-sortable="true">Creado</th>
						    </tr>
						    </thead>
						</table>
					</div>
				</div>
			</div>
		</div><!--/.row-->	
	
@endsection