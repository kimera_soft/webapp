<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class PlantationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('plantations')->insert([
        	'name' => 'Chinola', 
            'min_temperature' => 23.00,
            'max_temperature' => 25.45, 
            'min_moisture' => 300, 
            'max_moisture' => 400, 
            'user_id' => 1,
            'created_at' => '2016-04-16 07:50:00'
    	]);
    }
}
