<?php

use Illuminate\Database\Seeder;

class DevicesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('devices')->insert([
        	'name' => 'Monte 1',
        	'latitude' => 17.22166,
        	'longitude' => 17.2321567,
        	'user_id' => 1,
            'plantation_id' => 1,
            'created_at' => '2016-04-16 07:59:00'
    	]);

        DB::table('devices')->insert([
            'name' => 'Monte 2',
            'latitude' => 17.231566,
            'longitude' => 17.231567,
            'user_id' => 1,
            'plantation_id' => 1,
            'created_at' => '2016-04-16 07:59:00'
        ]);
    }
}
